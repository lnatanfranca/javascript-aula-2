import { gods } from './deuses.js';

// QUESTÃO 1

gods.forEach(god => console.log(`${god.name} ${god.features.length}`));

// QUESTÃO 2

gods.forEach(function(god, index) {
    if (god.roles.some(role => role === 'Mid')) {
        console.log(typeof god);
        console.log(`${index}: ${JSON.stringify(god)}`);
    }
})

// QUESTÃO 3

const sortedGods = gods.sort(function(a, b){
    if (a.pantheon < b.pantheon) return -1;
    else if (a.pantheon > b.pantheon) return 1;
    else return 0;
})

sortedGods.forEach((god, index) => console.log(`${index}: ${JSON.stringify(god)}`));

// QUESTÃO 4

const strGods = gods.map(function(god){
    return `${god.name} (${god.class})`;
})

strGods.forEach((god, index) => console.log(`${index}: "${god}"`));